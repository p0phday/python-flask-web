from flask import Flask
from flask import render_template

# 3.1-3.2
"""
    index.html:
    在templates文件夹中新建index.html和users.html
"""

app = Flask(__name__)

@app.route('/')
def newindex():
    return 'heelo'
@app.route('/31/<name>')
def index(name):
    render_template('31.html',name=name)

@app.route('/32',endpoint='users')
def users():
    goods = [
                {'name':'jack','age':18},
                {'name':'tom','age':20},
                {'name':'elliot','age':19}
            ]
    return render_template('31.html',goods=goods)

# 3.5
@app.route('/35',endpoint='jack')
def jack():
    return render_template('31.html')

@app.route('/36/',endpoint='tom')
def tom():
    return render_template('31.html')

# 自定义过滤器
def do_index_last(charater):
    cha = charater.upper().replace('J','%').replace('K','^').append('%')
    return cha

app.add_template_filter(do_index_last,'index_last')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

