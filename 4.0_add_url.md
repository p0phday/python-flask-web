### 1、4.1.2 add_url_rule的使用

#### 除了使用```@app.route```装饰器,我们还可以使用add_url_rule来绑定视图函数和url，下面给出代码。
``` python
from flask import Flask
from flask import redirect
from flask import url_for

app = Flask(__name__)

@app.route('/')
def index():
	return 'Hello world'

def page():
	return 'This add_url_rule'
	
app.add_url_rule('/page', endpoint='page',view_func=page)

```

### 2、类视图

#### 视图函数也可以结合类来实现，类实现的好处就是支持继承，可以将共性的东西放到父类中，类视图需要使用app.add_url_rule来注册，类视图分为标准视图和基于调度方法的类视图

##### 1、标准类视图

标准类视图有标准的写法
- 父类继承```flask.views.View```
- 子类调用```dispatch_request```进行返回，完成业务逻辑
- 子类需要使用```app.add_url_rule```进行注册，其中的```view_func```参数不能直接传入子类的名称，需要使用```as_view```做类方法转换
- 如果同时也指定了`endpoint`，endpoint会覆盖as_view指定的视图名，先endpoint名后as_view名

使用类视图，在父类中定义一个属性，在子类中完成各自的业务逻辑，同时都继承父类中的这一个属性:

``` python
from flask.views import View
from flask import Flask, render_template

app = Flask(__name__)

class Info(View): # 父类,继承flask.views.View
	def __init__(self):
		super().__init__() # 调用父类的init方法
		self.content = {
			'info': '这是需要继承的内容'
		}

class Page1(Info): # 子类，继承Info
	def dispatch_request(self):
		return render_template('index1.html', **self.content)

class Page2(Info):
	def dispatch_request(self):
		return render_template('index2.html', **self.content)

class Page3(Info):
	def dispatch_request(self):
		return render_template('index3.html', **self.content)


app.add_url_rule('/page1', endpoint='page1', view_func=Page1.as_view('page1'))
app.add_url_rule('/page2', endpoint='page2', view_func=Page2.as_view('page2'))
app.add_url_rule('/page3', endpoint='page3', view_func=Page3.as_view('page3'))

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0', port=5000)
```

分别定义三个子类的模板：

```xml
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>测试</title>
</head>
<body>
    <p>page1</p>
    <p>{{ ads }}</p>
</body>
</html>
```

```xml
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>测试</title>
</head>
<body>
    <p>page2</p>
    <p>{{ ads }}</p>
</body>
</html>
```

```xml
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>测试</title>
</head>
<body>
    <p>page3</p>
    <p>{{ ads }}</p>
</body>
</html>
```

查看结果，三个url的返回除了三个模板各自的内容外都需要输出父类的ads属性

  

![](https://upload-images.jianshu.io/upload_images/22206660-b449d1a3f0e45af1.png?imageMogr2/auto-orient/strip|imageView2/2/w/351/format/webp)

page1.png

  

![](https://upload-images.jianshu.io/upload_images/22206660-a7ab89c10cd9878b.png?imageMogr2/auto-orient/strip|imageView2/2/w/359/format/webp)

page2.png

  

![](https://upload-images.jianshu.io/upload_images/22206660-659eb77d83b845ef.png?imageMogr2/auto-orient/strip|imageView2/2/w/328/format/webp)

page3.png


---
