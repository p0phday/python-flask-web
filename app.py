from flask import Flask
from flask import flash
from flask import url_for
from flask import redirect
from flask import render_template
from markupsafe import escape

app = Flask(__name__)
app.secret_key = 'h43u@wHq9y432'

@app.route('/')
def index():
    return 'hello'
@app.route('/login/<string:name>')
def login(name):
    flash(f'hello escape(name)')
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(debug=True)
